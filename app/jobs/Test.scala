package jobs

import scala.concurrent.duration._
import akka.actor._
import play.api.libs.concurrent._

// Implicits
import play.api.Play.current
import play.api.libs.concurrent.Execution.Implicits._

class TickActor extends Actor {
  def receive = {
    case s => play.Logger.info(s"received $s")
  }
}

object Test { outer =>
  def run = {
    val Tick = "tick"
    val p = Props[TickActor]
    val tickActor = Akka.system.actorOf(p, name = "myactor")
    //Use system's dispatcher as ExecutionContext

    //This will schedule to send the Tick-message
    //to the tickActor after 0ms repeating every 50ms
    val cancellable =
      Akka.system.scheduler.schedule(0 milliseconds,
        50 milliseconds,
        tickActor,
        Tick)

    //This cancels further Ticks to be sent
    //cancellable.cancel()
  }
}

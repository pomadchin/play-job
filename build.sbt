import play.Project._

name := "types"

version := "1.0-SNAPSHOT"

scalaVersion := "2.10.4"

resolvers ++= Seq(
  "Sonatype OSS Releases"  at "http://oss.sonatype.org/content/repositories/releases/",
  "Sonatype Releases" at "http://oss.sonatype.org/content/repositories/releases",
  "Sonatype OSS Snapshots" at "http://oss.sonatype.org/content/repositories/snapshots/",
  "rillit-repository" at "http://akisaarinen.github.com/rillit/maven"
)

val monocleVersion = "0.5.0"

libraryDependencies ++= Seq(
  "org.squeryl"      % "squeryl_2.10" % "0.9.5-6",
  "com.cloudphysics" % "jerkson_2.10" % "0.6.3",
  "postgresql"       % "postgresql"   % "9.1-901.jdbc4",
  "org.scalaz"       %% "scalaz-core" % "7.0.6",
  "com.chuusai"      % "shapeless_2.10.4" % "2.0.0",
  "org.scalacheck"   %% "scalacheck"  % "1.11.5" % "test",
  "joda-time"        % "joda-time"    % "2.3",
  "org.joda"         % "joda-convert" % "1.6",
  "com.github.julien-truffaut"  %%  "monocle-core"    % monocleVersion,
  "com.github.julien-truffaut"  %%  "monocle-generic" % monocleVersion,
  "com.github.julien-truffaut"  %%  "monocle-macro"   % monocleVersion,     
  "com.github.julien-truffaut"  %%  "monocle-law"     % monocleVersion % "test",
  "fi.akisaarinen"              %% "rillit"           % "0.1.0"
)

playScalaSettings